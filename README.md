## Introduction
I've been trying to find a way to make MATLAB work in any Linux distribution for some time now, and I think I have finally found a way that should work on most distributions without hassle. It's still a work in progress, but the main part should be done.

It relies on Distrobox and (optionally) the Nix package manager.

## Dependencies
As of now, it only requires CURL and Podman. Additionally (and optionally) you can install Distrobox by yourself from your distribution's package manager. In case you don't want to do that, the first step of this installer will install the Nix package manager for you and install Distrobox.

## How to use
After downloading the official MATLAB installer from Mathworks, unzip it and copy the whole repository inside the unzipped folder. Then just run the script and follow the steps :)

```bash
cd matlab_official_installer
git clone https://gitlab.com/adriabrucortes/matlab-installer-for-linux.git
cd matlab-installer-for-linux
./matlab-universal.sh
```

### Important!
- This installer automatically creates a folder in your home directory called "MATLAB". **Install MATLAB in that folder** because it is needed for the desktop launcher to work!
- During the installation, the MATLAB installer will give you the option to create an optional symlink. **Leave that option unticked (unselected)**.


## Annotations
- The MATLAB app will probably not appear until you log out and log back in.
- When you launch the MATLAB app, a terminal window will appear. **DO NOT CLOSE IT** or it will close MATLAB aswell. The terminal window needs to run in the background.