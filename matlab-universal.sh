#!/bin/bash

function matlab() {

	# Install MATLAB
	mkdir /home/$USER/MATLAB
	distrobox create -i ubuntu -n matlab -Y
	distrobox enter matlab -- "sudo apt -y install xorg && ../install -destinationFolder /home/$USER/MATLAB"

	# Create launcher for 
	touch /home/$USER/MATLAB/matlab-launcher.sh
	echo "#!/bin/bash" >>/home/$USER/MATLAB/matlab-launcher.sh
	echo "distrobox enter matlab -- /home/$USER/MATLAB/bin/matlab" >>/home/$USER/MATLAB/matlab-launcher.sh
	chmod +x /home/$USER/MATLAB/matlab-launcher.sh

	# Preparing in case of conflicts or installer restart
	mkdir /home/$USER/.local/share/applications
	rm /home/$USER/.local/share/applications/matlab.desktop

	# Create desktop file with icon
	cp desktop/matlab-icon.png /home/$USER/MATLAB/matlab-icon.png
	cp desktop/matlab.desktop /home/$USER/.local/share/applications/matlab.desktop

	echo "Exec=bash /home/$USER/MATLAB/matlab-launcher.sh" >>/home/$USER/.local/share/applications/matlab.desktop
	echo "Icon=/home/$USER/MATLAB/matlab-icon.png" >>/home/$USER/.local/share/applications/matlab.desktop

}

# Dependencies: Podman, CURL

# Script
echo "############################################################"
echo "########################## BEWARE ##########################"
echo "############################################################"
echo "THIS SCRIPT ASSUMES IT IS INSIDE THE MATLAB INSTALLER FOLDER"
echo "Is it? (Y = Yes | Any = Abort)"
read act_path

if [[ $act_path = "Y" || $act_path = "y" ]]; then
	echo "Good :)"

else
	exit

fi

echo ""
echo "This script installs the Nix package manager in order to install Distrobox"
echo ""

echo "Is this step 1 or step 2 of installation (if you don't know, it's probably step 1)"
echo " 1 | 2"
read step

if [[ $step = 1 ]]; then

	echo ""
	echo "Do you want to make it a single-user installation (recommended if you don't plan on using nix for anything else)"
	echo "or multi-user (useful for managing more packages but requires shell restart)?"
	echo ""
	echo "Any key = Abort (default) | S = Single-user | M = Multi-user | I = Distrobox already installed"

	read NIX

	if [[ $NIX = "S" || $NIX = "s" ]]; then
		sh <(curl -L https://nixos.org/nix/install) --no-daemon
		. /home/$USER/.nix-profile/etc/profile.d/nix.sh
		echo ". /home/$USER/.nix-profile/etc/profile.d/nix.sh" >>/home/$USER/.bashrc
		nix-env -iA nixpkgs.distrobox
		matlab

	elif [[ $NIX = "M" || $NIX = "m" ]]; then
		sh <(curl -L https://nixos.org/nix/install) --daemon
		echo "Restart the shell (close and reopen the terminal) and rerun the script but this time type '2' when asked for the step"

	elif [[ $NIX = "I" || $NIX = "i" ]]; then
		echo "Already installed"
		matlab

	else
		echo "Operation cancelled"
		exit

	fi

elif [[ $step = 2 ]]; then
	nix-env -iA nixpkgs.distrobox
	matlab
fi
